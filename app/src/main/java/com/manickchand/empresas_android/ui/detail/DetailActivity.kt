package com.manickchand.empresas_android.ui.detail

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.manickchand.empresas_android.R
import com.manickchand.empresas_android.model.Enterprise
import com.manickchand.empresas_android.util.BASE_URL
import com.manickchand.empresas_android.util.loadImage
import com.manickchand.empresas_android.util.showToast
import kotlinx.android.synthetic.main.activity_detail.*

class DetailActivity : AppCompatActivity() {

    companion object {
        private const val EXTRA_ENTERPRISE = "EXTRA_ENTERPRISE"

        fun getStartIntent(context: Context, enterprise: Enterprise): Intent {
            return Intent(context, DetailActivity::class.java).apply {
                putExtra(EXTRA_ENTERPRISE, enterprise)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        toolbar.setNavigationOnClickListener {
            finish()
        }

        val enterprise: Enterprise? = intent.getParcelableExtra(EXTRA_ENTERPRISE) ?: return
        if(enterprise!=null){
            title = enterprise.enterprise_name

            tv_description_enterprise_detail.text = enterprise.description

            loadImage(iv_enterprise_detail, enterprise.photo)

        }else{
            showToast(resources.getString(R.string.error_enterprise_detail))
            finish()
        }
    }
}

