package com.manickchand.empresas_android.ui.login

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.manickchand.empresas_android.base.BaseViewModel
import com.manickchand.empresas_android.model.LoginModel
import com.manickchand.empresas_android.model.UserModel
import com.manickchand.empresas_android.repository.IServiceRetrofit
import com.manickchand.empresas_android.util.TAG_DEBUC
import kotlinx.coroutines.launch


class LoginViewModel(private val service: IServiceRetrofit) : BaseViewModel(){

    val loginLiveData = MutableLiveData<UserModel>()

    fun login(loginModel: LoginModel){

        launch {
            loading.value = true

            try {
                val response = service.login(loginModel)

                val token = response.headers()["access-token"]
                val client = response.headers()["client"]
                val uid = response.headers()["uid"]

                Log.i(TAG_DEBUC, " token: $token")
                Log.i(TAG_DEBUC, " client: $client")
                Log.i(TAG_DEBUC, " token: $uid")

                val userModel = response.body()!!

                userModel.accessToken = response.headers()["access-token"]
                userModel.client = response.headers()["client"]
                userModel.uid = response.headers()["uid"]

                loginLiveData.value = userModel
                hasErrorLiveData.value = false

            }catch (t:Throwable){
                hasErrorLiveData.value = true
                Log.i(TAG_DEBUC, "[error] Login: ${t.message}")

            }finally {
                loading.value = false
            }
        }
    }
}