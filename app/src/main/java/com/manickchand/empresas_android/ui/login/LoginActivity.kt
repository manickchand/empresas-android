package com.manickchand.empresas_android.ui.login

import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.manickchand.empresas_android.BaseApp
import com.manickchand.empresas_android.R
import com.manickchand.empresas_android.model.LoginModel
import com.manickchand.empresas_android.ui.main.MainActivity
import com.manickchand.empresas_android.util.showToast
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.loading.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class LoginActivity : AppCompatActivity() {

    private val loginViewModel by viewModel<LoginViewModel>()

    companion object {
        var pwdVisible:Boolean=false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        loginViewModel.hasErrorLiveData.observe(this, Observer {
            if(it){
                showToast( resources.getText(R.string.error_login).toString())
            }
        })

        loginViewModel.loading.observe(this, Observer {
            if(it){
                fl_loading.visibility = View.VISIBLE
                btn_login.visibility = View.GONE
            }else{
                fl_loading.visibility = View.GONE
                btn_login.visibility = View.VISIBLE
            }
        })

        loginViewModel.loginLiveData.observe(this, Observer {userModel ->
            if(userModel.success){
                BaseApp.currentUser = userModel
                startActivity(MainActivity.getStartIntent(this))
                finish()
            }else{
                showToast( resources.getText(R.string.error_field).toString())
            }
        })

        btn_login.setOnClickListener{
            if(this.validateData()){
                loginViewModel.login(LoginModel(et_email_login.text.toString(),et_password_login.text.toString()))
            }else{
                showToast( resources.getText(R.string.error_field).toString())
            }
        }


        iv_hide_pwd.setOnClickListener{
           showPwd()
        }
    }

    fun showPwd(){

        et_password_login.transformationMethod = if (pwdVisible){
             PasswordTransformationMethod.getInstance()
        }else{
             HideReturnsTransformationMethod.getInstance()
        }

        pwdVisible = pwdVisible.not()

    }

    private fun validateData() = android.util.Patterns.EMAIL_ADDRESS.matcher(et_email_login.text.toString()).matches() && et_password_login.text.toString().isNotEmpty()

}
