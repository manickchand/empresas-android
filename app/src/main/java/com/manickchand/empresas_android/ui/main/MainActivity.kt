package com.manickchand.empresas_android.ui.main

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.SearchView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.manickchand.empresas_android.R
import com.manickchand.empresas_android.model.Enterprise
import com.manickchand.empresas_android.ui.detail.DetailActivity
import com.manickchand.empresas_android.util.showToast
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.loading.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : AppCompatActivity(){

    private val mainViewModel by viewModel<MainViewModel>()
    private var mList:MutableList<Enterprise> = ArrayList()

    companion object {
        fun getStartIntent(context: Context): Intent {
            return Intent(context, MainActivity::class.java)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        searchListener()
        setupRecyclerView()

        mainViewModel.enterpriseLiveData.observe(this, Observer {listEnterprise ->

            if(listEnterprise.isEmpty()){
                tv_msg_main.text = resources.getString(R.string.enterprise_list_empty)
                mList.clear()
                tv_msg_main.visibility = View.VISIBLE

            }else{
                mList.addAll(listEnterprise)
                tv_msg_main.visibility = View.GONE
            }

            rv_enterprises.adapter!!.notifyDataSetChanged()
        })

        mainViewModel.loading.observe(this, Observer {
            if(it){
                fl_loading.visibility = View.VISIBLE
            }else{
                fl_loading.visibility = View.GONE

            }
        })

        mainViewModel.hasErrorLiveData.observe(this, Observer {
            if(it){
                showToast( resources.getText(R.string.error_get_enterprises).toString())
            }
        })

    }

    private fun searchListener(){
        sv_search_enterprise.setOnQueryTextListener(object : SearchView.OnQueryTextListener {

            override fun onQueryTextChange(newText: String) = false

            override fun onQueryTextSubmit(query: String): Boolean {
                mainViewModel.searchEnterprise(query)
                return false
            }

        })
    }

    private fun setupRecyclerView(){

        with(rv_enterprises){

            layoutManager = LinearLayoutManager(this@MainActivity,  RecyclerView.VERTICAL, false)
            setHasFixedSize(true)

            adapter = MainAdapter(context, mList){ enterprise ->
                val intent = DetailActivity.getStartIntent(this@MainActivity, enterprise)
                this@MainActivity.startActivity(intent)
            }
        }
    }

}
