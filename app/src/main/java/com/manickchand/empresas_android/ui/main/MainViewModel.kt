package com.manickchand.empresas_android.ui.main

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.manickchand.empresas_android.BaseApp
import com.manickchand.empresas_android.base.BaseViewModel
import com.manickchand.empresas_android.model.Enterprise
import com.manickchand.empresas_android.repository.IServiceRetrofit
import com.manickchand.empresas_android.util.TAG_DEBUC
import kotlinx.coroutines.launch

class MainViewModel(private val service: IServiceRetrofit) : BaseViewModel(){

    val enterpriseLiveData = MutableLiveData<List<Enterprise>>()

    fun searchEnterprise(name: String){

        launch {
            loading.value = true

            try {
                val response = service.searchEnterprise(
                                                        BaseApp.currentUser!!.accessToken ?: "",
                                                            BaseApp.currentUser!!.client ?: "",
                                                            BaseApp.currentUser!!.uid ?: "",
                                                                name)

                enterpriseLiveData.value = response.body()!!.enterprises ?: null
                hasErrorLiveData.value = false


            }catch (t:Throwable){
                hasErrorLiveData.value = true
                Log.i(TAG_DEBUC, "[error] searchEnterprise: ${t.message}")

            }finally {
                loading.value = false
            }
        }
    }

}