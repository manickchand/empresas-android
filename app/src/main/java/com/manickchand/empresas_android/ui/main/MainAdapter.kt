package com.manickchand.empresas_android.ui.main

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.manickchand.empresas_android.R
import com.manickchand.empresas_android.model.Enterprise
import com.manickchand.empresas_android.util.BASE_URL
import com.manickchand.empresas_android.util.loadImage
import kotlinx.android.synthetic.main.activity_detail.*
import kotlinx.android.synthetic.main.item_enterprise.view.*

class MainAdapter(context: Context,
                  list: List<Enterprise>,
                  private val onItemClickListener:((enterprise: Enterprise) -> Unit) ) : RecyclerView.Adapter<MainAdapter.MyViewHolder?>() {

    private var mContext =context
    private var mList = list
    private var lastPosition = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(mContext).inflate(R.layout.item_enterprise,parent,false)
        return MyViewHolder(view,onItemClickListener)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bindEnterprise(mList[position])
        setAnimation(holder.itemView, position)
    }

    private fun setAnimation(viewToAnimate: View, position: Int) {
        if (position > lastPosition) {
            val animation: Animation =
                AnimationUtils.loadAnimation(mContext, android.R.anim.slide_in_left)
            viewToAnimate.startAnimation(animation)
            lastPosition = position
        }
    }

    override fun getItemCount() = mList.count()

    inner class MyViewHolder(itemView:View,
                             private val onItemClickListener: ((enterprise:Enterprise) -> Unit)) :RecyclerView.ViewHolder(itemView){

        private var ivEnterprise: ImageView = itemView.iv_enterprise
        private var tvNameEnterprise: TextView = itemView.tv_name_enterprise
        private var tvCountryEnterprise: TextView = itemView.tv_country_enterprise
        private var tvTypeEnterprise: TextView = itemView.tv_type_enterprise

        fun bindEnterprise(enterprise:Enterprise) {

            loadImage(ivEnterprise, enterprise.photo)

            tvNameEnterprise.text = enterprise.enterprise_name
            tvCountryEnterprise.text = enterprise.country
            tvTypeEnterprise.text = enterprise.enterprise_type.enterprise_type_name

            itemView.setOnClickListener{
                onItemClickListener.invoke(enterprise)
            }
        }
    }
}