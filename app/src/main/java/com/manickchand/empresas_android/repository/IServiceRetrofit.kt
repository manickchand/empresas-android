package com.manickchand.empresas_android.repository

import com.manickchand.empresas_android.model.Enterprises
import com.manickchand.empresas_android.model.LoginModel
import com.manickchand.empresas_android.model.UserModel
import retrofit2.Response
import retrofit2.http.*

interface IServiceRetrofit {

    @POST("users/auth/sign_in")
    suspend fun login( @Body loginModel: LoginModel): Response<UserModel>

    @GET("enterprises")
    suspend fun searchEnterprise(@Header("access-token") accessToken:String,
                                 @Header("client") client:String,
                                 @Header("uid") uid:String,
                                @Query("name") name:String): Response<Enterprises>

}