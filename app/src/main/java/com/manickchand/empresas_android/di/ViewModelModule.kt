package com.manickchand.empresas_android.di

import com.manickchand.empresas_android.ui.login.LoginViewModel
import com.manickchand.empresas_android.ui.main.MainViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module


val viewModelModule = module {
    viewModel { LoginViewModel(get()) }
    viewModel { MainViewModel(get()) }
}