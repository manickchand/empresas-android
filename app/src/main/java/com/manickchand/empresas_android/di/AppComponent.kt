package com.manickchand.empresas_android.di

val appComponent = listOf(
    viewModelModule,
    networkModule
)