package com.manickchand.empresas_android

import android.app.Application
import com.manickchand.empresas_android.di.appComponent
import com.manickchand.empresas_android.model.UserModel
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class BaseApp: Application(){

    override fun onCreate() {
        super.onCreate()
        configureDI()
    }

    private fun configureDI() = startKoin {
        androidContext(this@BaseApp)
        modules(appComponent)
    }

    companion object{
        var currentUser: UserModel? = null
    }
}