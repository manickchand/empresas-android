package com.manickchand.empresas_android.util

import android.widget.ImageView
import com.bumptech.glide.Glide
import com.manickchand.empresas_android.R


fun loadImage(imageView: ImageView, imageUrl: String?) {
    if(imageUrl.isNullOrEmpty().not()) {
        try {
            //IMAGE URL ERROR
            val url = BASE_URL + imageUrl
            Glide.with(imageView.context)
                .load(url)
                .placeholder(R.drawable.placeholder)
                .into(imageView)
        } catch (e: Exception) {
        }
    }
}