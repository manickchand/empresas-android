package com.manickchand.empresas_android.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Enterprise(
    var id:Int?,
    var enterprise_name:String?,
    var description:String?,
    var country:String?,
    var enterprise_type: EnterpriseType,
    var photo:String?
): Parcelable

@Parcelize
data class EnterpriseType(
    var id:Int?,
    var enterprise_type_name:String?
): Parcelable

data class Enterprises(var enterprises:List<Enterprise>)
