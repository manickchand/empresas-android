package com.manickchand.empresas_android.model

data class LoginModel  (
    var email:String,
    var password:String
)