package com.manickchand.empresas_android.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class UserModel  (
    var investor:Investor,
    var enterprise:Enterprise,
    var success:Boolean,
    var accessToken:String?,
    var client:String?,
    var uid:String?
): Parcelable

@Parcelize
data class Investor(
    var id:Int?,
    var investor_name:String?,
    var email:String?,
    var country:String?,
    var balance: Double?,
    var photo: String?
): Parcelable

